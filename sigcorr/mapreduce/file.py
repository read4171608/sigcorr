import sys

from typing import List
from datetime import datetime
from multiprocessing import Process
from multiprocessing import Queue

import h5py
from tqdm import tqdm

from sigcorr.config import CFG
from sigcorr.tools.utils import produce_batch_ranges
from sigcorr.mapreduce.map_reducers import Calculator
from sigcorr.mapreduce.map_reducers import Reducer


def h5_batch_mapreduce(input_file: str, fields: List[str], batchsize: int, mapper: Calculator, reducer: Reducer):
    """
    Read samples from ``input_file`` and apply ``mapper`` to batches of size ``batchsize``,
    then aggregate processed batches with ``reducer``.

    Number of parallel processes is defined in the configuration: ``sigcorr.config.CFG.FITTER.bfit_poolsize``.

    You can either change number of processes by setting the new number explicitly
    in the code or use environment variables at runtime:

    .. code::

        SIGCORR_FITTER_bfit_poolsize=10 python3 your_script.py

    Parameters
    ----------
        input_file : str
            path to the input HDF5 file.
        fields : List[str]
            which fields to extract and pass to the mapper.
        batchsize : int
            size of batches split along the first dimension of each extracted ``field``.
        mapper : Calculator
            instance of the :py:class:`~sigcorr.mapreduce.map_reducers.Calculator` that processes batches
        reducer : Reducer
            instance of the :py:class:`~sigcorr.mapreduce.map_reducers.Reducer` that aggregates batches

    Returns
    -------
        result : Iterator[Tuple]
            Intermediate results of the aggregation yielded as tuple: ``(intermediate_result, num_processed)``.

            The last element of the iterator is the final result of the processing.

    Examples
    --------

    Example of computing the average local significance of a set of samples stored in an hdf5 file:

    .. testcode::

        from sigcorr.mapreduce.file import h5_batch_mapreduce
        from sigcorr.tools.utils import get_last_from_iter
        from sigcorr.mapreduce.map_reducers import SigsCalc
        from sigcorr.mapreduce.map_reducers import BatchStats1Reduce

        result_iterator = h5_batch_mapreduce(f"{DOCS_ROOT}/data/Asimov1D/hyy_tutorial.h5",
                                             ["b_loglikes", "sb_loglikes", "sb_params"],
                                             10,
                                             SigsCalc(),
                                             BatchStats1Reduce())
        resulting_bs, num_processed = get_last_from_iter(result_iterator)
        resulting_bs.get_mean()

    .. seealso::
        :doc:`/tutorial/batch_multiprocessing` tutorial, :py:mod:`sigcorr.map_reducers`,
        :py:class:`sigcorr.mapreduce.map_reducers.SigsCalc`,
        :py:class:`sigcorr.mapreduce.map_reducers.BatchStats1Reduce`

        :doc:`/tutorial/fitting` tutorial to learn more about the configuration.

        Helper: :py:func:`sigcorr.tools.utils.get_last_from_iter`
    """
    in_q = Queue()
    out_q = Queue()
    ref_seed = int(datetime.now().timestamp())//2
    processes = [Process(target=_h5_process_batches,
                         args=(in_q, input_file, fields, mapper, out_q))
                 for i in range(CFG.FITTER.bfit_pool_size)]
    [p.start() for p in processes]
    num_batches = 0
    with h5py.File(input_file, "r", swmr=True) as fin:
        n_samples = fin[fields[0]].shape[0]
    for brange in produce_batch_ranges(n_samples, batchsize):
        num_batches += 1
        in_q.put(brange)
    [in_q.put(None) for _ in processes]
    results_iterator = _h5_batch_iterresults(out_q, batchsize, num_batches)
    try:
        yield from reducer.reduce_batches(results_iterator, n_samples)
        [p.join() for p in processes]
    finally:
        [p.kill() for p in processes]


def _h5_process_batches(q_in, input_file, fields, mapper, q_out):
    with h5py.File(input_file, "r", swmr=True) as fin:
        while True:
            brange = q_in.get()
            if brange is None:
                return
            samples = [fin[field][brange, ...] for field in fields]
            res = mapper.process_batch(*samples)
            q_out.put(res)


def _h5_batch_iterresults(batch_res_q, approx_batchsize, num_batches):
    for _ in tqdm(range(num_batches),
                  total=num_batches, unit_scale=approx_batchsize, file=sys.stderr):
        one_batch_res = batch_res_q.get()
        yield one_batch_res
