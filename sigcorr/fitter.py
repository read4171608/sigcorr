import sys
from multiprocessing import Process
from multiprocessing import JoinableQueue
from multiprocessing import shared_memory
from itertools import repeat
import numpy as np
import scipy as sp
from scipy import optimize  # pylint: disable=unused-import # noqa: F401
import h5py
from tqdm import tqdm

from sigcorr.config import CFG
from sigcorr.models.model import AbstractModel
from sigcorr.tools.utils import produce_batch_ranges


class Fitter:
    """The fit manager that uses likelihoods provided by ``model`` for each point of the scan grid
    ``scan_xs`` to produce a significance surface. Stores the data in an HDF5 file.
    """
    CFG = CFG.FITTER

    class FinishProcessing:
        pass

    def __init__(self, model: AbstractModel, scan_xs: np.ndarray):
        """
        Parameters
        ----------
            model : AbstractModel
                model that provides the likelihoods to be used by Fitter
            scan_xs : np.ndarray
                array of model parameters. The last dimension defines a vector of the parameters for one grid point, and
                the rest of the dimensions define the grid.

                Example of the shape for 1 param ``(p1_size, 1)``,
                2 params ``(p1_size, p2_size, 2)``, 3 params ``(p1_size, p2_size, p3_size, 3)``.

        """
        self.model = model
        self.scan_xs = scan_xs

    def fit_b_loglikes(self, bg_samples: np.ndarray, output_f: str):
        """ Fit background likelihoods for each of the input background samples.

        Resulting HDF5 file ``output_f`` will contain the following fit information.

            b_loglikes : of shape ``(num_samples,)``
              value of the background maximum log-likelihood for each sample.
            b_params : of shape ``(num_samples, num_bg_model_params)``
              vector of the parameters that maximize the likelihood for each bg sample.
            b_params_vars : of shape ``(num_samples, num_bg_model_params)``
              vector of the variances of the parameters that maximize the likelihood for each bg sample.
            b_successes : of shape ``(num_samples,)``
                contains 1 for successful fit and 0 for unsuccessful fit for each bg sample.

        Parameters
        ----------
            bg_samples : np.ndarray
                array of background samples of the shape ``(num_samples, one_sample_grid_size)``.
            output_f : str
                name of the output HDF5 file the fitted curves will be stored into.
        """
        output_fields = {
            "b_loglikes": bg_samples.shape[0],
            "b_params": (bg_samples.shape[0], self.model.b_guess.shape[0]),
            "b_params_vars": (bg_samples.shape[0], self.model.b_guess.shape[0]),
            "b_successes": bg_samples.shape[0],
        }
        sys.stderr.write("Fitting B\n")
        input_queue = JoinableQueue()
        output_queue = JoinableQueue()
        bg_shmem = shared_memory.SharedMemory(create=True, size=bg_samples.nbytes)
        bg_samples_spec = {"shape": bg_samples.shape, "dtype": bg_samples.dtype}
        bg_sharr = np.ndarray(**bg_samples_spec, buffer=bg_shmem.buf)
        bg_sharr[...] = bg_samples
        processes = [Process(target=self.b_fit_worker, args=(self.model, input_queue,
                                                             bg_samples_spec, bg_shmem.name, output_queue))
                     for i in range(self.CFG.bfit_pool_size)]
        [p.start() for p in processes]
        write_process = Process(target=self.writer_process, args=(output_fields, output_queue, output_f, input_queue))
        write_process.start()
        sys.stderr.write("start pushing samples to B queue\n")
        for bg_sample_slice in produce_batch_ranges(bg_samples.shape[0], self.CFG.bfit_batchsize):
            input_queue.put(bg_sample_slice)
        sys.stderr.write("send FinishProcessing to workers and wait\n")
        [input_queue.put(self.FinishProcessing()) for _ in range(self.CFG.bfit_pool_size)]
        input_queue.join()
        [p.join() for p in processes]
        sys.stderr.write("send FinishProcessing to writer and wait\n")
        output_queue.put(self.FinishProcessing())
        output_queue.join()
        write_process.join()
        sys.stderr.write("done B\n")
        bg_shmem.close()
        bg_shmem.unlink()
        [p.close() for p in processes]

    @classmethod
    def b_fit_worker(cls, model, input_queue, bg_samples_spec, bg_samples_mem_name, output_queue):
        shmem = shared_memory.SharedMemory(name=bg_samples_mem_name)
        bg_samples = np.ndarray(**bg_samples_spec, buffer=shmem.buf)
        while True:
            cur_range = input_queue.get()
            if isinstance(cur_range, cls.FinishProcessing):
                input_queue.task_done()
                return
            cur_samples = bg_samples[cur_range]
            b_loglikes = np.zeros(cur_samples.shape[0])
            b_params = np.zeros((cur_samples.shape[0], model.b_guess.shape[0]))
            b_params_vars = np.zeros((cur_samples.shape[0], model.b_guess.shape[0]))
            b_successes = np.zeros(cur_samples.shape[0])
            for sample_i, sample in enumerate(cur_samples):
                loglike, params, params_vars, success = cls.one_b_loglike_fit(model, sample)
                b_loglikes[sample_i] = loglike
                b_params[sample_i] = params
                b_params_vars[sample_i] = params_vars
                b_successes[sample_i] = success
            output_queue.put((cur_range, {
                "b_loglikes": b_loglikes,
                "b_params": b_params,
                "b_params_vars": b_params_vars,
                "b_successes": b_successes,
            }))
            input_queue.task_done()
        shmem.close()

    @classmethod
    def one_b_loglike_fit(cls, model, sample):
        res = sp.optimize.minimize(model.b_minus_loglike, model.b_guess, args=(sample,),
                                   jac=model.b_minus_loglike_grad,
                                   hess=model.b_minus_loglike_hess,
                                   method=cls.CFG.optim_method, options=cls.CFG.optim_options)
        p = res.x
        p_var = np.diagonal(np.linalg.pinv(model.b_minus_loglike_hess(p, sample)))
        return -model.b_minus_loglike(p, sample), p, p_var, res.success

    def fit_sb_loglikes(self, bg_samples: np.ndarray, output_f: str):
        """ Fit signal+background likelihoods for each of the input background samples.

        Resulting HDF5 file ``output_f`` will contain the following fit information.

            sb_loglikes : of shape ``(num_samples, p1_size, p2_size, ...)``
              value of the signal+background maximum log-likelihood for each sample for each scan grid point.
            sb_params : of shape ``(num_samples, p1_size, p2_size, ..., num_sb_model_params)``
              vector of the parameters that maximize the likelihood for each bg sample for each scan grid point.
            sb_params_vars : of shape ``(num_samples, p1_size, p2_size, ..., num_sb_model_params)``
              vector of the variances of the parameters that maximize the likelihood for each bg sample
              for each scan grid point.
            sb_successes : of shape ``(num_samples, p1_size, p2_size, ..., num_sb_model_params)``
                contains 1 for successful fit and 0 for the unsuccessful fit for each bg sample
                for each scan grid point

        Parameters
        ----------
            bg_samples : np.ndarray
                array of background samples of the shape ``(num_samples, one_sample_grid_size)``.
            output_f : str
                name of the output HDF5 file the fitted curves will be stored into.
        """
        output_fields = {
            "sb_loglikes": (bg_samples.shape[0], *self.scan_xs.shape[:-1]),
            "sb_params": (bg_samples.shape[0], *self.scan_xs.shape[:-1], self.model.sb_guess.shape[0]),
            "sb_params_vars": (bg_samples.shape[0], *self.scan_xs.shape[:-1], self.model.sb_guess.shape[0]),
            "sb_successes": (bg_samples.shape[0], *self.scan_xs.shape[:-1]),
        }
        sys.stderr.write("Fitting S+B\n")
        input_queue = JoinableQueue()
        output_queue = JoinableQueue()
        bg_shmem = shared_memory.SharedMemory(create=True, size=bg_samples.nbytes)
        bg_samples_spec = {"shape": bg_samples.shape, "dtype": bg_samples.dtype}
        bg_sharr = np.ndarray(**bg_samples_spec, buffer=bg_shmem.buf)
        bg_sharr[...] = bg_samples
        processes = [Process(target=self.sb_fit_worker, args=(self.model, input_queue, self.scan_xs,
                                                              bg_samples_spec, bg_shmem.name, output_queue))
                     for i in range(self.CFG.sbfit_pool_size)]
        [p.start() for p in processes]
        write_process = Process(target=self.writer_process, args=(output_fields, output_queue, output_f, input_queue))
        write_process.start()
        sys.stderr.write("start pushing samples to SB queue\n")
        for bg_sample_slice in produce_batch_ranges(bg_samples.shape[0], self.CFG.sbfit_batchsize):
            input_queue.put(bg_sample_slice)
        sys.stderr.write("send FinishProcessing to workers and wait\n")
        [input_queue.put(self.FinishProcessing()) for _ in range(self.CFG.sbfit_pool_size)]
        input_queue.join()
        [p.join() for p in processes]
        sys.stderr.write("send FinishProcessing to writer and wait\n")
        output_queue.put(self.FinishProcessing())
        output_queue.join()
        write_process.join()
        sys.stderr.write("done SB\n")
        bg_shmem.close()
        bg_shmem.unlink()
        [p.close() for p in processes]

    @classmethod
    def sb_fit_worker(cls, model, input_queue, scan_xs, bg_samples_spec, bg_samples_mem_name, output_queue):
        shmem = shared_memory.SharedMemory(name=bg_samples_mem_name)
        bg_samples = np.ndarray(**bg_samples_spec, buffer=shmem.buf)
        while True:
            cur_range = input_queue.get()
            if isinstance(cur_range, cls.FinishProcessing):
                input_queue.task_done()
                return
            cur_samples = bg_samples[cur_range]
            sb_loglikes = np.zeros((cur_samples.shape[0], *scan_xs.shape[:-1]))
            sb_params = np.zeros((cur_samples.shape[0], *scan_xs.shape[:-1], model.sb_guess.shape[0]))
            sb_params_vars = np.zeros((cur_samples.shape[0], *scan_xs.shape[:-1], model.sb_guess.shape[0]))
            sb_successes = np.zeros((cur_samples.shape[0], *scan_xs.shape[:-1]))
            for sample_i, sample in enumerate(cur_samples):
                for loc_i in np.ndindex(scan_xs.shape[:-1]):
                    loc = scan_xs[loc_i]
                    if np.all(np.isnan(loc)):
                        continue
                    loglike, params, params_vars, success = cls.one_sb_loglike_fit(model, sample, loc)
                    sb_loglikes[(sample_i, *loc_i)] = loglike
                    sb_params[(sample_i, *loc_i)] = params
                    sb_params_vars[(sample_i, *loc_i)] = params_vars
                    sb_successes[(sample_i, *loc_i)] = success
            output_queue.put((cur_range, {
                "sb_loglikes": sb_loglikes,
                "sb_params": sb_params,
                "sb_params_vars": sb_params_vars,
                "sb_successes": sb_successes,
            }))
            input_queue.task_done()
        shmem.close()

    @classmethod
    def one_sb_loglike_fit(cls, model, sample, signal_x):
        res = sp.optimize.minimize(model.sb_minus_loglike, model.sb_guess, args=(sample, signal_x),
                                   jac=model.sb_minus_loglike_grad,
                                   hess=model.sb_minus_loglike_hess,
                                   method=cls.CFG.optim_method, options=cls.CFG.optim_options)
        p = res.x
        p_var = np.diagonal(np.linalg.pinv(model.sb_minus_loglike_hess(p, sample, signal_x)))
        return -model.sb_minus_loglike(p, sample, signal_x), p, p_var, res.success

    @classmethod
    def writer_process(cls, output_fields, input_q, output_f, workers_q):
        with h5py.File(output_f, "a") as fout:
            total_iters = 0
            for k, shape in output_fields.items():
                fout[k] = np.zeros(shape)
            fout.swmr_mode = True
            total_iters = np.product(shape)
            iter_batch_size = total_iters // (shape if isinstance(shape, int) else shape[0])
            progress_bar = tqdm(repeat(None), total=total_iters, file=sys.stderr)
            with progress_bar as pb:
                while True:
                    res = input_q.get()
                    if isinstance(res, cls.FinishProcessing):
                        sys.stderr.write(f"finish writing {input_q.qsize()}\n")
                        input_q.task_done()
                        return
                    cur_range, cur_data = res
                    for k, v in cur_data.items():
                        fout[k][cur_range] = v
                    pb.set_postfix({"writer_qsize": input_q.qsize(), "workers_qsize": workers_q.qsize()})
                    pb.update(iter_batch_size*(cur_range.stop - cur_range.start))
                    input_q.task_done()
