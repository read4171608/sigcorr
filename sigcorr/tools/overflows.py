import numpy as np


def overflows_along_zero_ax(data: np.ndarray, thresholds: np.ndarray) -> np.ndarray:
    """Determine whether each dataset from the input exceeds provided thresholds.

    Parameters
    ----------
        data : np.ndarray
            Array of datasets, where the first dimension (0-axis) of the ``data`` enumerates the datasets.
        thresholds : np.ndarray
            1-dimensional array of thresholds. Each dataset will be tested against each threshold.

    Returns
    -------
        overflows : np.array
            Array of signatures ``True/False``, that tells for each dataset
            whether it exceeded thresholds. Output shape is ``(num_datasets, num_thresholds)``.

    Examples
    --------

    .. testcode::

        import numpy as np
        from sigcorr.tools.overflows import overflows_along_zero_ax

        data = np.array([[-1.2, 2.1, 3.5],
                         [-2., -1., 0.1]])
        thresholds = np.array([-1., 2., 5., -5])

        data_exceeds = overflows_along_zero_ax(data, thresholds)
        # [[True, True, False, True],
        #  [True, False, False, True]]
    """
    return (data[..., np.newaxis] > thresholds).any(axis=tuple(range(1, data.ndim)))
