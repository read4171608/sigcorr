from typing import List
from typing import Optional

import numpy as np
from scipy import ndimage as ndi
import scipy as sp
from scipy import interpolate  # noqa; pylint: disable=unused-import


def derivative_along_axes(arr: np.array,
                          xs_axes: List[np.array],
                          der_axes: Optional[List[int]] = None) -> np.ndarray:
    """Compute derivative of a function along a subset of axes defined on a grid.

    This method tries to apply cubic spline interpolation if possible, otherwise, it falls back to
    finite differences.

    Parameters
    ----------
    arr : np.array
        Input array of function values evaluated on the grid.
    xs_axes : List[np.array]
        List of coordinates, 1D array per axis. The grid on which the function was evaluated to get ``arr``.
    der_axes : Optional[List[int]]
        List of orders of the derivatives along each axis

    Returns
    -------
    np.ndarray
        Array with derivatives applied along axes. The result has the same shape as the original array.

    Examples
    --------
    .. testcode::

        import numpy as np
        from sigcorr.tools.derivative import derivative_along_axes

        xs = np.linspace(0, np.pi, 10)
        ys = np.linspace(-np.pi/2, np.pi/2, 10)
        arr = np.outer(np.sin(ys), np.cos(xs))  # each row corresponds to some value of y, where f(x, y) = cos(x)*sin(y)
        d_arr = derivative_along_axes(arr, [ys, xs], [0, 1])  # derivative along the second axis (xs)
        # we end up with array df(x, y) = -sin(y)*sin(x)
    """
    if der_axes is None:
        der_axes = [0 for _ in xs_axes]
    try:
        return spline3_der_along_axes(arr, xs_axes, der_axes)
    except NotImplementedError:
        return finite_diff_der_along_axes(arr, xs_axes, der_axes)


def finite_diff_der_along_axes(arr, xs_axes, der_axes):
    res = arr
    for i, xs in enumerate(xs_axes):
        for _ in range(der_axes[i]):
            res = finite_diff_der1_along_axis(res, xs, i)
    return res


def finite_diff_der1_along_axis(arr, xs, ax):
    # kernel is reversed because convolution will reverse it back
    res = ndi.convolve1d(arr, [1, 0, -1], mode="nearest", axis=ax)
    dx = ndi.convolve(xs, [1, 0, -1], mode="nearest")
    return res/dx


def spline3_der_along_axes(arr, xs_axes, der_axes):
    if len(xs_axes) != 2:
        raise NotImplementedError()
    return spline3_der_along_2axes(arr, *xs_axes, *der_axes)


def spline3_der_along_2axes(arr, xs, ys, dx, dy):
    spline_obj = sp.interpolate.RectBivariateSpline(xs, ys, arr)
    return spline_obj(xs, ys, dx=dx, dy=dy)
