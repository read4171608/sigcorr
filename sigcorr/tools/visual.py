import numpy as np
import scipy as sp
from scipy import stats


def set_plt_reasonable_defaults(plt):
    plt.rcParams["figure.figsize"] = (12, 8)
    plt.rcParams['figure.constrained_layout.use'] = True
    plt.rcParams["xtick.direction"] = "in"
    plt.rcParams["xtick.major.size"] = 8
    plt.rcParams["xtick.major.width"] = 1.6
    plt.rcParams["xtick.minor.width"] = 0.8
    plt.rcParams["xtick.minor.size"] = 4
    plt.rcParams["ytick.direction"] = "in"
    plt.rcParams["ytick.major.width"] = 1.6
    plt.rcParams["ytick.minor.width"] = 0.8
    plt.rcParams["ytick.major.size"] = 8
    plt.rcParams["ytick.minor.size"] = 4
    plt.rcParams["font.size"] = 16
    plt.rcParams["lines.linewidth"] = 3
    plt.rcParams["lines.markersize"] = 5
    plt.rcParams["savefig.dpi"] = 300/2.4
    plt.rcParams["savefig.transparent"] = False
    plt.rcParams["savefig.facecolor"] = "white"


def plot_norm_pulls(ax, cov_diff, subtract_diag=False, bins=20):
    weights = None if not subtract_diag else (1. - np.eye(cov_diff.shape[0])).ravel()
    _, bins, _ = ax.hist(cov_diff.ravel(), bins=bins, density=True, weights=weights);
    mean, std = np.mean(cov_diff), np.std(cov_diff)
    fitted_mean, fitted_std = sp.stats.norm.fit(cov_diff.ravel(), loc=mean, scale=std)
    cdfs = sp.stats.norm(loc=fitted_mean, scale=fitted_std).cdf(bins)
    ax.stairs((cdfs[1:] - cdfs[:-1])/(bins[1:] - bins[:-1]), bins, color="red", label=f"N({fitted_mean:.4f}, {fitted_std:.4f})")
