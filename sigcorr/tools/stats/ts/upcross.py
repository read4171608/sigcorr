import numpy as np


def propagate_upcross(ref_ts: np.ndarray, ref_upcross: np.ndarray, noise_dof: int, target_ts: np.ndarray) -> np.ndarray:
    r"""Propagate the average number of the test statistic up-crossings between thresholds.

    When doing a one dimensional likelihood scan, a test statistic can be constructed for each point of the scanning
    grid. In the asymptotic regime every point of this curve follows a chi-squared distribution with ``noise_dof``
    degrees of freedom.

    An important characteristic of such a curve is the number of points where this curve crosses a threshold
    upwards, and is a function of the threshold.

    For one particular curve one can use :py:mod:`~sigcorr.tools.upcross` to count the up-crossings. When done
    on many curves from one statistical model, the average number of up-crossings can be estimated for some threshold.

    For any model, when the average number of up-crossings is known (``ref_upcross``)
    at some threshold (``ref_ts``), it is possible to predict the average number of up-crossings
    at any other level ``target_ts`` [1]_:

    .. math::

        <N(c_1)> = <N(c_0)> \left( \frac{c_0}{c_1} \right)^{(s-1)/2} \mathrm{e}^{-(c_1 - c_0)/2},


    where :math:`c_0` is a reference threshold ``ref_ts``, :math:`c_1` is the target threshold ``target_ts``, ``s`` is
    the number of degrees of freedom of the test statistic (``noise_dof``), and
    :math:`<N(c)>` is the average number of up-crossings at the threshold :math:`c`.

    Parameters
    ----------
        ref_ts : np.ndarray
            Reference test statistic threshold.
        ref_upcross : np.ndarray
            Average number of up-crossings known for the ``ref_ts`` threshold.
        noise_dof : int
            The test statistic follows the chi-squared distribution with this number of degrees of freedom
            in each point of the scanning grid.
        target_ts : np.ndarray
            An array of thresholds we want the prediction of the average number of up-crossings for.

    Returns
    -------
        propagated_upcross : np.ndarray
            Average number of up-crossings at ``target_ts`` thresholds.

    References
    ----------
    .. [1] E. Gross and O. Vitells "Trial factors for the look elsewhere effect in high energy physics,"
           Eur. Phys. J. C 70, 525–530 (2010). https://doi.org/10.1140/epjc/s10052-010-1470-8

    Examples
    --------

    .. testcode::

        import numpy as np
        from sigcorr.tools.stats.ts.upcross import propagate_upcross

        low_threshold = 1.2
        avg_upcross_low = 3.24
        noise_dof = 1
        target_thresholds = np.array([2,3,4,5.])
        avg_upcross_high = propagate_upcross(low_threshold, avg_upcross_low, noise_dof, target_thresholds)
        # array([2.17183695, 1.3172857 , 0.79897416, 0.48460233])

    .. seealso::

        :py:mod:`sigcorr.tools.upcross`

    """
    return ref_upcross*np.power(target_ts/ref_ts, (noise_dof-1)/2)*np.exp(-(target_ts - ref_ts)/2)
