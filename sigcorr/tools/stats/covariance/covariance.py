import numpy as np
import scipy as sp
from scipy import stats


def get_partial_covariances(sigs):
    means = np.mean(sigs, axis=0)
    means_times_sig = np.einsum("...i,j->...ij", sigs, means)
    return np.einsum("...i,...j->...ij", sigs, sigs) \
        - means_times_sig - np.transpose(means_times_sig, axes=(0, 2, 1)) \
        + np.tensordot(means, means, axes=0)[np.newaxis, ...]


def cov_and_err(sigs):
    res_dim = sigs.shape[-1]
    cov = np.zeros((res_dim, res_dim))
    err = np.zeros((res_dim, res_dim))
    for i in range(res_dim):
        for j in range(i+1):
            cov[i, j], err[i, j] = cov_and_err_1d(sigs[:, i], sigs[:, j])
            cov[j, i], err[j, i] = cov[i, j], err[i, j]
    return cov, err


def cov_and_err_1d(x, y):
    xy_cov, _, _, xy_err = _common_cov_and_err_1d(x, y)
    return xy_cov, xy_err


def corr_and_err(sigs):
    res_dim = sigs.shape[-1]
    corr = np.zeros((res_dim, res_dim))
    err = np.zeros((res_dim, res_dim))
    for i in range(res_dim):
        for j in range(i+1):
            corr[i, j], err[i, j] = corr_and_err_1d(sigs[:, i], sigs[:, j])
            corr[j, i], err[j, i] = corr[i, j], err[i, j]
    return corr, err


def corr_and_err_1d(x, y):
    xy_cov, x_std, y_std, xy_err = _common_cov_and_err_1d(x, y)
    std_prod = x_std*y_std
    return xy_cov/std_prod, xy_err/std_prod


def _common_cov_and_err_1d(x, y):
    x_mean = x.mean()
    x_std = x.std()
    y_mean = y.mean()
    y_std = y.std()
    x_c = x - x_mean
    y_c = y - y_mean
    xy_c = x*y - (x*y).mean()
    xy_cov = (x_c*y_c).mean()
    xy_var = (x_mean*y_std)**2 + (y_mean*x_std)**2 + (xy_c**2).mean() \
        - 2*x_mean*(xy_c*y_c).mean() - 2*y_mean*(xy_c*x_c).mean() + 2*x_mean*y_mean*xy_cov
    return xy_cov, x_std, y_std, np.sqrt(xy_var/x.shape[0])


def cov_zmean(arr):
    return np.dot(arr, arr.T)*np.true_divide(1, arr.shape[0])


def corr_zmean(arr):
    arr_std = np.std(arr, axis=0)*np.true_divide(1, np.sqrt(arr.shape[0]))
    return cov_zmean(arr)/np.dot(arr_std, arr_std.T)
