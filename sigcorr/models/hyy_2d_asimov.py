import numpy as np
from sigcorr.models.hyy_2d import Hyy2D


class Hyy2DAsimov(Hyy2D):
    def get_bg_samples(self, num_samples):
        expected_b = self.expected_b(self.B0, self.BG_XSCALE)
        return expected_b.reshape(1, -1) \
             + self.BG_NOISE_STD * np.eye(expected_b.shape[0])
