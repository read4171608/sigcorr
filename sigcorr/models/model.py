from __future__ import annotations
from typing import List
import numpy as np


class AbstractModel:
    """Abstract class for |project| model

    Every model should implement the following set of operations:

        * model parameters + ``init()`` method for them
        * method for sampling background samples
        * background model, its derivatives, starting point for the optimization
        * signal + background model, its derivatives, starting point for the optimization

    Attributes
    ----------
    PARAMS : List[str]
        List of model param names. Parameters will be stored to file when the model
        is dumped and restored back based on this list.
    """

    PARAMS: List[str] = []

    def __init__(self, xs: np.ndarray):
        """
        Properties defined here are required, and are used by other utils in |project|. However,
        they can also be defined in :py:meth:`init`.

        Parameters
        ----------
        xs : np.ndarray
            A grid on which the background model is defined.

        Examples
        --------
        .. code-block::

            def __init__(self, xs):
                # Required
                self.xs = xs

                # Required. Initial guess for the background model parameters used in scipy optimize
                self.b_guess: np.ndarray

                # Required. Initial guess for the signal+background model parameters used in scipy optimize
                self.sb_guess: np.ndarray
        """
        self.xs = xs
        self.b_guess: np.ndarray = None
        self.sb_guess: np.ndarray = None

    def init(self) -> AbstractModel:
        """Post-initializer. Should always be called. Call it after values of the parameters were set.

        Defines some paramters that can be derived from the model parameters.

        Examples
        --------
        .. code-block::

            def init(self):
                self.b_guess = np.array([self.p1])
                self.sb_guess =  np.array([self.p1**2, self.p2/self.p1])
                return self

        """
        return self

    def get_bg_samples(self, num_samples: int) -> np.ndarray:
        """Produce a batch of background samples.

        Parameters
        ----------
        num_samples : int
            Number of samples to be produced.

        Returns
        -------
        np.ndarray
            First axis enumerates the samples, so has exactly ``num_samples`` elements,
            rest of the axes correspond to the shape of the background sample.

            Output shape: ``(num_samples, *self.xs.shape)``

        """
        raise NotImplementedError()

    def b_minus_loglike(self, p: np.ndarray, sample: np.ndarray):
        """Returns ``-log(likelihood ratio)`` for the background model.

        It is passed to ``sp.optimize`` to estimate model parameters with MLE.

        Parameters
        ----------
        p: np.ndarray
            Array of paramters that will be optimized for the background model.
            Has same shape as ``self.b_guess``.
        sample: np.ndarray
            One background sample for which the likelihood will be optimized
        """
        raise NotImplementedError()

    def b_minus_loglike_grad(self, p: np.ndarray, sample: np.ndarray):
        """Gradient for :py:meth:`b_minus_loglike`

           Passed to ``sp.optimize`` together with :py:meth:`b_minus_loglike`

           Parameters
           ----------
           * : * :py:meth:`b_minus_loglike`
        """
        raise NotImplementedError()

    def b_minus_loglike_hess(self, p: np.ndarray, sample: np.ndarray):
        """Hessian for :py:meth:`b_minus_loglike`

           Passed to ``sp.optimize`` together with :py:meth:`b_minus_loglike`

           Parameters
           ----------
           * : * :py:meth:`b_minus_loglike`
        """

    def sb_minus_loglike(self, p: np.ndarray, sample: np.ndarray, signal_x: np.ndarray) -> np.ndarray:
        """Returns ``-log(likelihood ratio)`` for the background + signal model

        It is passed to ``sp.optimize`` to estimate model parameters with MLE.

        Parameters
        ----------
        p: np.ndarray
            Array of paramters that will be optimized for the background model.
            Has same shape as ``self.sb_guess``.
        sample: np.ndarray
            One background sample for which the likelihood will be optimized
        signal_x: np.ndarray
            Value of the nuisance parameter not present under background hypothesis.
            If there are more than 1, ``signal_x`` can be a vector.
        """
        raise NotImplementedError()

    def sb_minus_loglike_grad(self, p: np.ndarray, sample: np.ndarray, signal_x: np.ndarray) -> np.ndarray:
        """Gradient for :py:meth:`sb_minus_loglike`

           Passed to ``sp.optimize`` together with :py:meth:`sb_minus_loglike`

           Parameters
           ----------
           * : * :py:meth:`sb_minus_loglike`
        """
        raise NotImplementedError()

    def sb_minus_loglike_hess(self, p: np.ndarray, sample: np.ndarray, signal_x: np.ndarray) -> np.ndarray:
        """Hessian for :py:meth:`sb_minus_loglike`

           Passed to ``sp.optimize`` together with :py:meth:`sb_minus_loglike`

           Parameters
           ----------
           * : * :py:meth:`sb_minus_loglike`
        """
        raise NotImplementedError()
