#!/bin/bash

BASEDIR=$(dirname "$0")
DATA_DIR="${BASEDIR}/data"

if [ ! -d "$DATA_DIR" ]; then
    git worktree add "$DATA_DIR" toy_data
fi

make -C ${BASEDIR}/img/
