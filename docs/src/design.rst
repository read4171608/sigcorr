Design
======

|project| provides a set of utilities that may be useful to study the trials factors for a large
variety of statistical models. Aside of MC toys fitting, this package implements in code formulas
from well known papers about the trials factor. The implementation was verified on millions of MC
toys and can serve as a basis for future studies.

Although |project| is flexible in what you can do with it, the package was developed with a goal in
mind to study the Asimov set of samples [1]_ and this guided its design in a way. Therefore, we find it
convenient to describe the structure of the framework on example of the trials factor computation.

Trials factor appears when signal model has nuisance parameters that are not present under the
background hypothesis. Therefore, statistical significance of the signal discovered in the
signal+background fit overestimates the actual significance of the discovery.

Model and Fitter (requires dev mode)
------------------------------------

The straightforward way to estimate the trials factor is to count how many significance surfaces,
that live on the manifold of these special nuisance parameters, exceed the local significance level.

For this, one needs to define:

* a :doc:`/api/model`
* ``sampling_grid``, a grid on which background samples will be sampled
* ``scan_grid``, a grid that defines values for the nuisance parameters that are only
  present in the signal+background hypothesis

More detailed explanation of the grid file format is given in :doc:`/tutorial/grids`.

First, instantiate the model with the ``sampling_grid``. Then, feed the model and the ``scan_grid`` to
:doc:`/api/fitter`. Fitter will use the model to produce MC toys, fit them and store the fitted
results to HDF5 file. CLI interface for these operations is implemented in the :src:`sigcorr-run` script
and described in :doc:`/tutorial/fitting`.

Readers and MapReducers
-----------------------

At this point we have some HDF5 files with fit results. We would like to compute some summary statistics
on these files. For example to set the upper bound for the trials factor we need to
compute the average number of up-crossings from thousands of samples, or we will need the covariance
matrix between these samples to estimate the trials factor in the GP approximation.

In |project| there is a
way to easily build a :doc:`batch multiprocessing pipeline </tutorial/batch_multiprocessing>` that will read data in
batches from the HDF5 file, distribute batches among many processes and then aggregate the results.

Similarly, for the GP approximation, it is possible to sample batches of data from the covariance matrix, and then
apply the same pipeline but to GP toys. In the next chapters we show examples of application of this approach in
:doc:`1D </tutorial/visualization_1d>` and :doc:`2D </tutorial/visualization_2d>`.

Trials factor, GP and Asimov set of samples
---------------------------------------------

To estimate trials factor in the brute force way, making lots of expensive likelihood fits is required,
but can be avoided. From the properties of the
likelihood fits and statistical tests it turns out that significance surfaces can be well
approximated by Gaussian process, where the covariance encapsulates all the information about the
signal and background hypotheses.

* One way to estimate the covariance matrix is to use the brute force toys (but we would need fewer samples than for
  the trials factor estimation), then to do the likelihood scans
  and to compute the covariance matrix on the grid. This method is straightforward, however, it is still computationally
  heavy. This approach is described in detail in :doc:`/tutorial/visualization_1d`.
* Alternatively, one can use Asimov set of background samples, that is specifically constructed
  to provide a good estimate for the covariance matrix. Find the step by step tutorial in
  :doc:`/tutorial/asimov_set_of_bg_samples`.

Both approaches result in the covariance matrix that can be used in different ways. Trials factor,
for example, can be estimated via sampling of GP toys.

Availability of the covariance matrix also simplifies the Gross and Vitells upper bound estimation.
The latter relies on the average number of up-crossings that can be estimated via sampling from the GP or
with the analytical expression that gives the up-crossings density of the Gaussian process from its covariance matrix.

Tools
-----

|project| also implements a large set of tools. While all the tools were developed with intention to be used standalone,
some high level functions of |project| (e.g. Readers and MapReducers) are built on top of them.

Historically the utilities became structured by specificity. The highest, least specific, layer
contains very generic methods that help to compute:

* :py:mod:`sigcorr.tools.derivative` - derivatives of smoothed data,
* :py:mod:`sigcorr.tools.upcross` - up-crossings,
* :py:mod:`sigcorr.tools.overflows` - exceedigs of threholds by curves and surfaces,
* :py:mod:`sigcorr.tools.euler_number` - Euler characteristics in various dimensions.

A level below consists of one module :py:mod:`sigcorr.tools.stats`. It contains submodules:

* :py:mod:`sigcorr.tools.stats.utils` - functions to compute test statistics from the values of the likelihoods,
* :py:mod:`sigcorr.tools.stats.batch_stats` - tooling for batch statistics, that allows to compute mean,
  standard deviation or covariances without loading entire dataset into RAM.

If we dig even deeper we will find a module that encapsulates some knowledge about statistical
properties of the Gaussian process (:py:mod:`sigcorr.tools.stats.gp`):

* :py:mod:`sigcorr.tools.stats.gp.sampling` - various ways to sample from a multivariate gaussian distribution,
* :py:mod:`sigcorr.tools.stats.gp.upcross` - estimating the average up-crossings from the covariance matrix of the Gaussian process,
* :py:mod:`sigcorr.tools.stats.gp.euler_number` - propagating Euler numbers of the sets of points
  of the significance surface that exceed a threshold (overflow sets).

Alongside with the Gaussian process there is a module to compute Euler number of the overflow sets of the test statistic,
when the test statistic distribution follows :math:`\chi^2` distribution with more than 1 d.o.f.:

* :py:mod:`sigcorr.tools.stats.ts.euler_number` - propagate Euler numbers of the overflow sets of the test statistic.

The Gaussian process and the test statistic modules are of the very high importance in |project|. They
were implemented based on the published scientific works, thoroughly tested and proved to work consistently together.

References
----------

.. [1] V. Ananyev and A. L. Read, "Gaussian Process-based calculation of look-elsewhere trials factor,"
       JINST 18 (2023) P05041, https://doi.org/10.1088/1748-0221/18/05/P05041
