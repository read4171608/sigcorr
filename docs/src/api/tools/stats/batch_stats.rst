Batch statistics
================

.. automodule:: sigcorr.tools.stats.batch_stats
   :undoc-members:
   :members: BatchStats1, BatchStats2
