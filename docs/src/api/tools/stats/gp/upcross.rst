Up-crossings
============

.. automodule:: sigcorr.tools.stats.gp.upcross
   :undoc-members:
   :members: gp_upcross_at_level
