Derivative
==========

.. automodule:: sigcorr.tools.derivative
   :undoc-members:
   :members: derivative_along_axes
