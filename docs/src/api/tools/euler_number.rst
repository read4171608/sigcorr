Euler number
============

.. automodule:: sigcorr.tools.euler_number
   :undoc-members:
   :members: euler_number_along_zero_ax
