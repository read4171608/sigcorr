MapReduce
=========

Split input data into batches and process in parallel.

Batches can be distributed onto many workers (mapped) to be processed in parallel with the help of
the function-orchestrator :py:func:`~sigcorr.mapreduce.file.h5_batch_mapreduce`. An instance of
:py:class:`~sigcorr.mapreduce.map_reducers.Calculator` defines the operations applied to every
batch. Then the results of processing of each batch are aggregated (reduced) into the final value
using :py:class:`~sigcorr.mapreduce.map_reducers.Reducer`.

A second orchestrator,
:py:func:`~sigcorr.mapreduce.gp.gp_batch_mapreduce`, was developed specifically to study
the Gaussian process approach to the modeling of significance surfaces. Instead of reading the
significance surfaces from an HDF5 file, :py:func:`~sigcorr.mapreduce.gp.gp_batch_mapreduce` generates
batches of Gaussian process samples using the Gaussian process covariance matrix as an input
parameter. The same :py:class:`~sigcorr.mapreduce.map_reducers.Calculator`
/ :py:class:`~sigcorr.mapreduce.map_reducers.Reducer` interface can be used to compute summary
statistics of Gaussian process samples.

The main steps of setting up the batch processing are:

#. Choose the ``reader``:

    * samples from an h5 file: :py:func:`~sigcorr.mapreduce.file.h5_batch_mapreduce`.

    * samples from the GP: :py:func:`~sigcorr.mapreduce.gp.gp_batch_mapreduce`.

#. Build the pipeline of the processors: :py:class:`~sigcorr.mapreduce.map_reducers.Calculator`.

#. Choose the aggreagator: :py:class:`~sigcorr.mapreduce.map_reducers.Reducer`.

#. Exhaust the reader (:py:func:`~sigcorr.tools.utils.get_last_from_iter`). When the ``reader`` is called it returns
   a generator that yields a tuple with incremental intermediate results from the batches and the number of processed
   samples (not batches!).

The full list of available calculators and reducers can be found in
:py:mod:`sigcorr.mapreduce.map_reducers`. For example, the most common Calculator is
:py:class:`~sigcorr.mapreduce.map_reducers.SigsCalc`. It translates a batch of likelihoods into
a batch of statistical significances corresponding to these likelihoods.

Some basic math functions can be applied to the batch (e.g. ``np.reshape``) using
:py:class:`~sigcorr.mapreduce.map_reducers.MathCalc`. Calculators also can be chained with
:py:class:`~sigcorr.mapreduce.map_reducers.ChainCalc`, so that one can get a batch of indicators
whether significance fields exceeds a particular threshold
(:py:class:`~sigcorr.mapreduce.map_reducers.OverflowsCalc`).

.. toctree::
   /api/mapreduce/batch_managers
   /api/mapreduce/map_reducers

.. seealso::
   :doc:`/tutorial/batch_multiprocessing` tutorial
