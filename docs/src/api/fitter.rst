Fitter
======

.. autoclass:: sigcorr.fitter.Fitter
   :members:
   :special-members: __init__
