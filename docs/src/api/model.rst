Model
=====

.. autoclass:: sigcorr.models.model.AbstractModel
   :members:
   :special-members: __init__
