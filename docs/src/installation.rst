Installation
============

Requirements
------------


The code was developed and run on Python 3.9.13, so any Python >= 3.9 should work. Older versions of Python
may also work but will require "dev mode" installation with a change in the ``python_requires`` option in :src:`setup.cfg`.

A set of packages that were used to build this documentation
are listed in `requirements.txt <requirements.txt>`_.

All figures and most of the code snippets in these docs are generated using |project|
during the documentation build.
We, therefore, believe this set of dependencies can be helpful if installation issues occur.

Installation modes
------------------

.. NOTE::
    |project| uses `google/jax <https://github.com/google/jax>`_ for the just-in-time compilation of the loss functions.
    To increase the precision of the calculations, special environment variables need to be set.
    We stored the variables in the :src:`env.sh` script
    and suggest to set them each time before you start working with |project|.

    You can explicitly run these commands before using |project|:

    .. literalinclude:: /../../env.sh
       :language: bash
       :linenos:

    In the development mode the script is available, so you can just ``source`` it:

    .. code-block:: bash

       source ./env.sh


1. Tools mode
-------------

*This mode is for you if likelihood scans are produced and ready for use and* |project| *has the functionality you need.*


It is possible to install the package directly from Gitlab:

.. code-block:: bash

    pip install git+https://gitlab.com/sigcorr/sigcorr.git

You can also specify the commit/tag/branch to install from:

.. code-block:: bash

    pip install git+https://gitlab.com/sigcorr/sigcorr.git@master#egg=sigcorr-9999


2. Development mode
-------------------

*This mode is for you if you need to add custom modules to the package.
For example, for fitting the toys,* |project| *needs to be extended with a new model custom to your analysis.*

To be able to extend the |project| live, it is more convenient to install it
in the development mode ``pip install -e``. Then any changes to the code base will immediately be reflected
on the next run:

.. code-block:: bash

    git clone https://gitlab.com/sigcorr/sigcorr.git
    cd sigcorr
    pip install -e .


Test your setup
---------------

Try running in the terminal the following command (you should see some help message):

.. code:: bash

    sigcorr-run -h

Alternatively, check that you can import something from the Python interpreter:

.. code::

    from sigcorr.tools.stats.utils import sig2pval
    print(sig2pval(3.))
