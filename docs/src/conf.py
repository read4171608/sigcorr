import os

project = 'SigCorr'
author = 'Victor Ananyev (vindex10), Alex L. Read'
extensions = ['sphinx.ext.autodoc', 'sphinx.ext.mathjax', 'sphinx.ext.viewcode',
              'sphinx.ext.extlinks', 'sphinx.ext.intersphinx', 'numpydoc',
              'matplotlib.sphinxext.plot_directive', 'sphinx.ext.autosectionlabel',
              'sphinx.ext.doctest']
autosectionlabel_prefix_document = True
highlight_language = 'python3'
autodoc_member_order = 'bysource'
numpydoc_show_class_members = False
html_theme = 'sphinx_rtd_theme'
autodoc_inherit_docstrings = False
plot_formats=["png", "hires.png"]
special_members = "__init__"
extlinks = {"src": ("https://gitlab.com/sigcorr/sigcorr/-/blob/master/%s", "/%s"),
            "toy_data": ("https://gitlab.com/sigcorr/sigcorr/-/blob/toy_data/%s", "toy_data/%s")}
intersphinx_mapping = {'python': ('https://docs.python.org/3', None),
                       'jax': ('https://jax.readthedocs.io/en/latest/', None),
                       'scipy': ('https://docs.scipy.org/doc/scipy/', None),
                       'skimage': ('https://scikit-image.org/docs/stable/', None),
                       'numpy': ('https://numpy.org/doc/stable/', None)}

_docs_root = os.path.dirname(os.path.abspath(__file__))
doctest_global_setup = f"""
DOCS_ROOT="{_docs_root}"
"""

rst_prolog = f"""
.. |project| replace:: {project}
"""
